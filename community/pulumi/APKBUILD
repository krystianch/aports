# Contributor: Hoang Nguyen <folliekazetani@protonmail.com>
# Contributor: Fraser Waters <frassle@gmail.com>
maintainer="Hoang Nguyen <folliekazetani@protonmail.com>"
pkgname=pulumi
pkgver=3.136.1
pkgrel=0
pkgdesc="Infrastructure as Code SDK"
url="https://pulumi.com/"
# Tests show that pulumi's plugin system doesn't work on any other platforms
arch="x86_64 aarch64"
license="Apache-2.0"
makedepends="go"
checkdepends="tzdata"
subpackages="
	$pkgname-bash-completion
	$pkgname-fish-completion
	$pkgname-zsh-completion
	$pkgname-language-go:_go
	$pkgname-language-nodejs:_nodejs
	$pkgname-language-python:_python
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/pulumi/pulumi/archive/v$pkgver.tar.gz"
options="net" # download Go modules

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	local _goldflags="-X github.com/pulumi/pulumi/sdk/v${pkgver%%.*}/go/common/version.Version=v$pkgver"
	mkdir -p "$builddir"/bin

	cd "$builddir"/sdk/go
	go generate ./...
	go build -C pulumi-language-go -v \
		-o "$builddir"/bin/pulumi-language-go \
		-ldflags "$_goldflags"

	for lang in python nodejs; do
		cd "$builddir"/sdk/$lang/cmd/pulumi-language-$lang
		go build -v \
			-o "$builddir"/bin/pulumi-language-$lang \
			-ldflags "$_goldflags"
	done

	cd "$builddir"/pkg
	go build -v \
		-o "$builddir"/bin/pulumi \
		-ldflags "-X github.com/pulumi/pulumi/pkg/v${pkgver%%.*}/version.Version=v$pkgver" \
		./cmd/pulumi

	cd "$builddir"
	for shell in bash fish zsh; do
		./bin/pulumi gen-completion $shell > pulumi.$shell
	done
}

check() {
	cd "$builddir"/pkg

	# PULUMI_ACCEPT=true is required for TestProgressEvents test
	# PULUMI_DISABLE_AUTOMATIC_PLUGIN_ACQUISITION is set to work around GitHub API's rate limit (ref: https://github.com/pulumi/pulumi/issues/15900)
	# TestProjectNameOverrides, TestProjectNameDefaults tests depend on pulumi-language-yaml
	# TestPulumiNewSetsTemplateTag test requires PULUMI_ACCESS_TOKEN to be set for running `pulumi new` without prompting
	# TestAzureCloudManager, TestAzureKeyVault* tests require Azure CLI installed
	# TestGoModEdits hardcode pulumi/ as the root path to search for sdk/go.mod file.
	# shellcheck disable=2046
	PULUMI_ACCEPT=true \
		PULUMI_SKIP_UPDATE_CHECK=true \
		PULUMI_DISABLE_AUTOMATIC_PLUGIN_ACQUISITION=true \
		go test \
		-skip 'TestPulumiNewSetsTemplateTag/python|TestGoModEdits|TestProjectName*|TestAzure*' \
		$(go list ./... | grep -v -E 'github.com/pulumi/pulumi/pkg/v3/codegen/(dotnet|nodejs|python)')
}

package() {
	install -Dm755 bin/pulumi -t "$pkgdir"/usr/bin/

	install -Dm644 pulumi.bash \
		"$pkgdir"/usr/share/bash-completion/completions/pulumi
	install -Dm644 pulumi.fish \
		"$pkgdir"/usr/share/fish/vendor_completions.d/pulumi.fish
	install -Dm644 pulumi.zsh \
		"$pkgdir"/usr/share/zsh/site-functions/_pulumi
}

_go() {
	pkgdesc="$pkgdesc (Go language provider)"

	install -Dm755 "$builddir"/bin/pulumi-language-go -t "$subpkgdir"/usr/bin/
}

_nodejs() {
	pkgdesc="$pkgdesc (NodeJS language provider)"
	depends="cmd:node"

	install -Dm755 "$builddir"/bin/pulumi-language-nodejs -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-analyzer-policy \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/nodejs/dist/pulumi-resource-pulumi-nodejs \
		-t "$subpkgdir"/usr/bin/
}

_python() {
	pkgdesc="$pkgdesc (Python language provider)"
	depends="python3"

	install -Dm755 "$builddir"/bin/pulumi-language-python -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/cmd/pulumi-language-python-exec -t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-analyzer-policy-python \
		-t "$subpkgdir"/usr/bin/
	install -Dm755 "$builddir"/sdk/python/dist/pulumi-resource-pulumi-python \
		-t "$subpkgdir"/usr/bin/
}

sha512sums="
2b8b6f711c66014edcd20bb2b53fd8a0ba2b0c1d8ed6f5d1be91f1adf440a18deb85dd7f6a96d3ed8b8e9ee1bd097e463ed714ceb962691d58ba4d593cf084ea  pulumi-3.136.1.tar.gz
"
