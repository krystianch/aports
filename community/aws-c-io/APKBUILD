# Contributor: Holger Jaekel <holger.jaekel@gmx.de>
# Contributor: fossdd <fossdd@pwned.life>
maintainer="fossdd <fossdd@pwned.life>"
pkgname=aws-c-io
pkgver=0.14.19
pkgrel=0
pkgdesc="Module for the AWS SDK for C handling all IO and TLS work for application protocols"
url="https://github.com/awslabs/aws-c-io"
# s390x: aws-c-common
# ppc64le: fails tests
# arm*: fails a bunch of tests / segfaults
arch="all !armhf !armv7 !ppc64le !s390x"
license="Apache-2.0"
makedepends="
	aws-c-cal-dev
	aws-c-common-dev
	cmake
	openssl-dev
	s2n-tls-dev
	samurai
	"
subpackages="$pkgname-dev"
source="$pkgname-$pkgver.tar.gz::https://github.com/awslabs/aws-c-io/archive/refs/tags/v$pkgver.tar.gz"
options="net" # required for tests to make connections

case "$CARCH" in
aarch64)
	# broken on new networking builder setup
	options="$options !check"
	;;
esac

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		local crossopts="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CFLAGS="$CFLAGS -flto=auto" \
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -B build -G Ninja \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=/usr/lib \
		-DBUILD_SHARED_LIBS=True \
		-DCMAKE_BUILD_TYPE=None \
		-DBUILD_TESTING="$(want_check && echo ON || echo OFF)" \
		$crossopts
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

dev() {
	default_dev
	amove usr/lib/aws-c-io
}

sha512sums="
613c058bd8fc6cc1e3a108cbd1afe920517b88869749b2a3fb1e71cf9ce8b6923c769b4314522fe025e71841e868b26370ec0d7bdc7bac8cfbbd5612541e09af  aws-c-io-0.14.19.tar.gz
"
